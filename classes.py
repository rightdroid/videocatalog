from kivy.uix.screenmanager import Screen
from kivy.uix.video import Video
from kivy.uix.button import Button
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import StringProperty, ObjectProperty
from kivy.graphics import Color, Rectangle, BorderImage


# -----------------------
# START CLASS EXTENSIONS
# -----------------------

class ScreenWithBG(Screen):
    def __init__(self, **kwargs):
        super(ScreenWithBG, self).__init__(**kwargs)
        with self.canvas.before:
            self.bg_img = Rectangle(
                size=(self.width, self.height),
                pos=(self.x, self.y),
                source='src/img/bg.png')
            
        self.bind(pos=self.update_bg_image)
        self.bind(size=self.update_bg_image)

    def update_bg_image(self, *args):
        self.bg_img.pos = self.pos
        self.bg_img.size = self.size


# extending Video class to enable EOS continuity on pausing
class VideoWithEOS(Video):
    def on_state(self, instance, value):
        if not self._video:
            return
        if value == 'play':
            if self.eos:
                self._video.stop()
                self._video.position = 0.
                self._video.eos = False
            self.eos = False
            self._video.play()
        elif value == 'pause':
            self._video.pause()
        else:
            self._video.stop()
            self._video.position = 0
            self._video.eos = False
    def _on_eos(self, *largs):
        # logger.info('center: {} :::::::::::::::: _on_eos'.format(self.center))
        # if(self.options['eos'] == 'loop'):
        #     logger.warning('FOO center: {} :::::::::::::::: _on_eos'.format(self.center))
        #     self.eos = False
        #     self._video.play()
        if self._video.eos != 'loop':
            # logger.info('_on_eos trigger')
            self.state = 'stop'
            self.eos = True

# extending button class to enable Screen changing
class ScreenButton(Button):
    screenmanager = ObjectProperty()
    targetscreen = StringProperty()

    def on_release(self, *args):
        super(ScreenButton, self).on_press(*args)
        self.screenmanager.current = self.targetscreen
    
        
# extending button class to enhance passing on button value
class KeyboardButton(Button):     
    pressed_value = StringProperty()
    kb_type = StringProperty()
    def on_press(self, *args):
        super(KeyboardButton, self).on_press(*args)
        self.pressed_value = self.pressed_value
        self.kb_type = self.kb_type


class KBWrapperBox(RelativeLayout):
    def __init__(self, **kwargs):
        super(KBWrapperBox, self).__init__(**kwargs)
        # with self.canvas:
            # self.rect = Rectangle(source='src/img/kb_border_color.png', pos=(self.pos[0], self.pos[1]), size=self.size)
        with self.canvas.after:
            self.border_img = BorderImage(
                size=(self.width + 100, self.height + 100),
                pos=(self.x - 50, self.y - 50),
                border=(30, 30, 30, 30),
                source='src/img/kb_border_color.png')
            
        self.bind(pos=self.update_border_image)
        self.bind(size=self.update_border_image)

    def update_border_image(self, *args):
        self.border_img.pos = self.pos
        self.border_img.size = self.size


# class KeyboardButtonWrapperLayout(BoxLayout):
    
# -----------------------
# END CLASS EXTENSIONS
# -----------------------
