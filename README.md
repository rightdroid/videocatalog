# README #

## Work In Progress

Video Cataloging written in python using kivy framework.
Because none of the free ones satisfied my personal requirements.


### ToDo:


* tags
* filtering/search
* options/configuration panel
* collect videos from subdirectories
* thumbnail view/list view
* thumbnail storyboard creation ([ffmpegthumbnailer](https://github.com/dirkvdb/ffmpegthumbnailer)?)
* remember last played video
* list of recently played videos
* make generated metainfo persistent (currently rewritten every time video collection changes)
* make it pretty

### Done:

* collect videos from single directory
* create thumbnails of collected videos and show them on selection
* generate metainfo from collected videos
* remember last play position
* navigate playhead via arrows (left, right - moderate increment; up, down - minimum increment)
* take screencap of video (s shortcut key)