# configuration file with constants and globals
import cfg
import os
# os.environ['KIVY_IMAGE'] = 'sdl2'
# os.environ['KIVY_VIDEO'] = 'gstreamer'
import sys
_window_width = cfg._main_monitor_w
_window_height = cfg._main_monitor_h

from kivy.config import Config
Config.set('kivy', 'keyboard_mode', 'system')
Config.set('graphics', 'width', _window_width)
Config.set('graphics', 'height', _window_height)
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'top', '50')
Config.set('graphics', 'left', '50')

# Config.set('graphics', 'fbo', 'software')
# Config.set('graphics', 'multisamples', '0')
# Config.set('kivy', 'log_level', 'trace')


import time, datetime, subprocess, threading, math, logging
import traceback, requests, smtplib, json, codecs, configparser
from pprint import pprint
from random import randint
from queue import Queue
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import multiprocessing as mp
# from multiprocessing import Process, Queue
from subprocess import Popen
from os import listdir
from os.path import isfile, join
from functools import partial

from kivy.lang import Builder
from kivy.uix.progressbar import ProgressBar
from kivy.uix.screenmanager import ScreenManager, Screen, RiseInTransition, FadeTransition, NoTransition, WipeTransition

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.uix.bubble import Bubble
from kivy.animation import Animation
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.image import Image, AsyncImage
# from kivy.uix.videoplayer import VideoPlayer
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.uix.video import Video
from kivy.uix.videoplayer import VideoPlayer
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.graphics import Color, Rectangle, BorderImage, Rotate
from kivy.uix.textinput import TextInput
from kivy.clock import Clock, mainthread
from kivy.graphics.context_instructions import PopMatrix, PushMatrix
from kivy.uix.listview import ListView
from kivy.uix.stacklayout import StackLayout
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.dictadapter import DictAdapter
from kivy.adapters.simplelistadapter import SimpleListAdapter
from kivy.uix.listview import ListItemButton, ListItemLabel
from kivy.uix.filechooser import FileChooser 



# DEBUGGING
from kivy.modules import inspector
from kivy.core.window import Window
from memory_profiler import profile

import io
from kivy.core.image import Image as CoreImage



# custom classes
import classes

# metainfo initialization, sanity checks for zips, thumbs and videos
import setup


# Set up logging
if(cfg._logging):
    logger = logging.getLogger('main')
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
    

    controls_logfile = cfg.LOG_PATH+'main.log'
    if not os.path.exists(controls_logfile):
        open(controls_logfile, 'w').close() 
    mainhdlr = logging.FileHandler(controls_logfile)
    mainhdlr.setFormatter(formatter)
    mainhdlr.setLevel(logging.INFO)
    
    logger.addHandler(mainhdlr) 
    logger.setLevel(logging.DEBUG)
    
    if(cfg._debug):
        # show logging info on console
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
else:
    logger = logging.getLogger('main')
    nullhdlr = logging.NullHandler()
    logger.addHandler(nullhdlr) 


# --------------- Declare vars --------------- 
# success_smiley = '(^ ω ^)'
# shareConfirm_popup = Popup(title="Jagatud!",
#   content=Label(text=str(success_smiley), color=(1,0,0,1)),
#   pos_hint={'right': 1, 'top' : 1},
#   size=(200, 100),
#   auto_dismiss=False,
#   size_hint=(None, None),
#   background_color=(0, 0, 0, 0))


logger.info('----------------------------------------  app start ---------------------------------------- ')


##########################################################################################
        ############################## START INITIALIZATION ##############################
##########################################################################################   
def init_meta():
    """
    Conducts sanity checks for videos_meta.ini file, repopulates on necessity.
    Creates new frames and zips them when video files are changed.
    Creates png thumbnails for videos.
    """
    logger.info('Entering setup')
    
    # ENSURE CORRECT METAINFORMATION
    global media
    media = setup.MediaMeta()
    all_metas = media.getMetas()
    # pprint(all_metas)
    media.fetch_thumbs()
    media.meta_sanity_check()
    # quit()
    if not setup.compare(cfg._video_files_wo_ext, cfg._thumb_files_wo_ext):
        for vid in cfg._video_files:
            if os.path.splitext(vid)[0] not in cfg._thumb_files_wo_ext:
                pprint(vid)
                media.generateThumb(vid)
    # quit()
    

init_meta()
##########################################################################################
        ############################## END INITIALIZATION ##############################
##########################################################################################   

class MyAdapter(ListAdapter):
    def __init__(self, **kwargs):
        super(MyAdapter, self).__init__(**kwargs)
        self.new_vid_str = ''

    # listview_id = NumericProperty(0)
    # owning_view = ObjectProperty(None)

    def __init__(self, **kwargs):
        # self.listview_id = kwargs['listview_id']
        super(MyAdapter, self).__init__(**kwargs)

    def on_selection_change(self, *args):
        if(self.selection):
            cfg._current_selection = os.path.splitext(self.selection[0].text)[0]
            
            new_selection_str = self.selection[0].text
            self.change_vid_player_src(new_selection_str)
        
    def change_vid_player_src(self, new_vid_str):
        cfg._new_vid = new_vid_str
        new_fullpath = cfg.VIDEO_SOURCE_PATH + new_vid_str
        self.new_vid_str = new_vid_str
        new_name = os.path.splitext(new_vid_str)[0]
        global root
        
        # check if new vid is not already selected vid
        if new_fullpath not in root.vid_player.source:
            pprint(':::::::::::::::change_vid_player_src')
            root.setLastVidSeek()
                
            root.vid_player.thumbnail = cfg.THUMB_PATH + os.path.splitext(new_vid_str)[0] + cfg._thumb_extension
            root.vid_player.source = new_fullpath
            root.vid_player.vid_name = new_vid_str
    
        

class RootWidget(RelativeLayout):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        
        self.mainWrapper = GridLayout(rows=2,size_hint=(1,1))
        self.add_widget(self.mainWrapper)
        
        # menubar
        self.menubar = StackLayout(size_hint=(1, None), size=(10,50))
        self.mainWrapper.add_widget(self.menubar)
        
        # buttons
        menu_buttons = [
        Button(text='File', size_hint=(None, .8)),
        Button(text='Options', size_hint=(None, .8)),
        Button(text='Copy', size_hint=(None, .8)),
        Button(text='Ext Player', size_hint=(None, .8), on_press=self.openInExternal),
        Button(text='Test', size_hint=(None, .8), on_press=self.testButton),
        ]
        
        for btn in (menu_buttons):
            self.menubar.add_widget(btn)
        
        
        self.main_view = GridLayout(cols=2)
        self.mainWrapper.add_widget(self.main_view)
        
        # list
        simple_list_adapter = MyAdapter(
            data=[str(index) for index in cfg._video_files],
            cls=ListItemButton,
            selection_mode='single',
            args_converter=self.args_converter
            )
        list_view = ListView(adapter=simple_list_adapter, size_hint_x=.4)
        self.main_view.add_widget(list_view)
        
        
        # VIDEO
        self.preview = RelativeLayout()
        self.vid_player = VideoPlayer(source='', options={'allow_stretch': True}, thumbnail=cfg.VIDEO_SOURCE_PATH + 'thumb.png')
        # self.vid_player.bind(on_frame=self.frameChangeCB)
        self.vid_player.bind(source=self.getLastVidSeek)
        self.vid_player.bind(state=partial(self.getLastVidSeek, 'state_call'))
        self.vid_player.bind(state=partial(self.setLastVidSeek, 'state_call'))
        
        self.preview.add_widget(self.vid_player)
        self.main_view.add_widget(self.preview)
        
            
            
    def setLastVidSeek(self, *args):
        # save last vid seek pos
        if(self.vid_player.source != '' and self.vid_player.state != 'stop'):
            last_vid = os.path.splitext(self.vid_player.vid_name)[0]
            setup.writeSingleToIni(last_vid, 'last_pos', self.vid_player._video.position / self.vid_player._video.duration)
            setup.readMetaIntoObject()
            pprint('writing last vid : {} : to pos: {}'.format(last_vid, cfg._vid_meta[last_vid]['last_pos']))
            # pprint('writing current pos: {}'.format(self.vid_player._video.position / self.vid_player._video.duration))
        
                    
    def getLastVidSeek(self, *args):
        global root
        if args[0] == 'state_call':
            pprint('state call: {}'.format(self.vid_player.state))
            if self.vid_player.state == 'pause':
                return False
                
        # pprint('this should not see')
        if self.vid_player._video is None:
            pprint('.')
            Clock.schedule_once(self.getLastVidSeek, -1)
            # return False
        else:    
            new_name = os.path.splitext(cfg._new_vid)[0]
            if(cfg._vid_meta[new_name]['last_pos'] != 0):
                # pprint('new video: {}'.format(new_name))
                pprint('getting seek to: {}'.format(cfg._vid_meta[new_name]['last_pos']))
                # pprint(cfg._vid_meta[new_name]['last_pos'])
                try:
                    self.vid_player._video.seek(float(cfg._vid_meta[new_name]['last_pos']))
                except Exception as e:
                    pprint('.')
                    Clock.schedule_once(self.getLastVidSeek, -1)
                    print('seek failed: {}'.format(e))
        
    def testButton(self, *args):
        pos = 110.5
        pprint('current seek: {}'.format(self.vid_player._video.position))
        pprint('setting pos to: {}'.format(pos))
        pprint(self.vid_player._video.position / self.vid_player._video.duration)
        # self.vid_player.state = 'pause'
        # self.vid_player._video.position = pos
        self.vid_player._video.seek(0.6)
        
    def openInExternal(self, *args):
        if(self.vid_player.source != ''):
            p = Popen([cfg.EXTENRAL_PLAYER, self.vid_player.source], shell=True)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        pprint(keycode[1])
        if keycode[1] == 'w':
            pprint('w')
        elif keycode[1] == 's':
            ts = time.time()
            ts_formated = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%y_%H-%M-%S')
            global root
            pprint(self.vid_player.children[1].children[0])
            
            # for child in self.vid_player.children:
                # print(child)
            # quit()
            self.vid_player.children[1].children[0].export_to_png(cfg.CAPS_PATH + os.path.splitext(root.vid_player.vid_name)[0] + '_' +ts_formated+'.png')
            pprint('screencap')
            
        elif keycode[1] == 'right':
            try:
                self.vid_player._video.seek((self.vid_player._video.position / self.vid_player._video.duration) + 0.005)
            except Exception as e:
                print('failed keyboard seek: {}'.format(e))
                
        elif keycode[1] == 'left':
            try:
                self.vid_player._video.seek((self.vid_player._video.position / self.vid_player._video.duration) - 0.005)
            except Exception as e:
                print('failed keyboard seek: {}'.format(e))
                
        elif keycode[1] == 'up':
            try:
                self.vid_player._video.seek((self.vid_player._video.position / self.vid_player._video.duration) + 0.0001)
            except Exception as e:
                print('failed keyboard seek: {}'.format(e))
                
        elif keycode[1] == 'down':
            try:
                self.vid_player._video.seek((self.vid_player._video.position / self.vid_player._video.duration) - 0.0001)
            except Exception as e:
                print('failed keyboard seek: {}'.format(e))
            
        elif keycode[1] == 'spacebar':
            try:
                if(self.vid_player.state == 'play'):
                    self.vid_player.state = 'pause'
                else:
                    self.vid_player.state = 'play'
            except Exception as e:
                print('failed keyboard play/pause: {}'.format(e))

        return False
        
        
    def args_converter(self, row_index, an_obj):
        return {'text': an_obj,
                'size_hint_y': None,
                'height': 45}

    def frameChangeCB(self, *args):
        pprint('framechange')

class VideoCatalogApp(App):
    def build(self):
        
        global root      
        root = RootWidget()
        
        # DEBUG, inspector
        if(cfg._inspector):
            inspector.create_inspector(Window, root)
            
        return root
        
    def on_stop(self):
        print('stopping')
        last_vid = os.path.splitext(root.vid_player.vid_name)[0]
        setup.writeSingleToIni(last_vid, 'last_pos', root.vid_player._video.position / root.vid_player._video.duration)
        killer = subprocess.Popen(['python', 'kill_all_python.py'], shell=True)
        killer.daemon = False

if __name__ == "__main__":
    # from kivy.base import runTouchApp
    # runTouchApp(MainView(width=800))
    VideoCatalogApp().run()