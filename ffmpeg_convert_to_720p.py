# convert to 720p with minimal quality loss
ffmpeg -i inputfile.mp4 -vf scale=-1:720 -c:v libx264 -crf 18 -preset slow -c:a copy outputfile.mp4