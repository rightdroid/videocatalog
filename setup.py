import cfg
import os, threading, subprocess, re, io, configparser, shutil, collections
from pprint import pprint
from os import listdir
from os.path import isfile, join
from zipfile import ZipFile
import shutil



createFramesZips = False


class MediaMeta():
    def __init__(self, **kwargs):
        super(MediaMeta, self).__init__(**kwargs)
 
        # fetch initial video files
        self.fetch_vids()
       # fetch initial thumbnail files
        self.fetch_thumbs()
       
        
        
        self.frames_dir = 'frames' + cfg.SEP
        # self.framerate = str(29.9)
        self.framerate = cfg._frames_fps
        self.scale = cfg._frames_scale
        self.frames_extension = cfg._frames_extension
        
        self.allMetas = []
        self.video_files = cfg._video_files
        
        # generate lists for later comparison
        self.vids_list_w_zip_ext = []
        self.vids_list_wo_ext = []
        for v in cfg._video_files:
            self.vids_list_w_zip_ext.append(v[:-4]+'.zip')
            self.vids_list_wo_ext.append(v[:-4])

    def fetch_vids(self, *args):
        cfg._video_files = [f for f in listdir(cfg.VIDEO_SOURCE_PATH) if isfile(join(cfg.VIDEO_SOURCE_PATH, f)) and f.endswith(cfg._video_extensions)]
        cfg._video_files_wo_ext = [os.path.splitext(f)[0] for f in listdir(cfg.VIDEO_SOURCE_PATH) if isfile(join(cfg.VIDEO_SOURCE_PATH, f)) and f.endswith(cfg._video_extensions)]
        if(cfg._vid_limit != False):
            cfg._video_files = cfg._video_files[:cfg._vid_limit]
            cfg._video_files_wo_ext = cfg._video_files_wo_ext[:cfg._vid_limit]
        
    def fetch_thumbs(self, *args):
        cfg._thumb_files = [f for f in listdir(cfg.THUMB_PATH) if isfile(join(cfg.THUMB_PATH, f)) and f.endswith(cfg._thumb_extension)]
        cfg._thumb_files_wo_ext = [os.path.splitext(f)[0] for f in listdir(cfg.THUMB_PATH) if isfile(join(cfg.THUMB_PATH, f)) and f.endswith(cfg._thumb_extension)]
    
        
    def generateFrames(self, vid_file):
        mp4_name = (vid_file[:-4])
        mp4_ext = (vid_file[-4:])
        # pprint.pprint(mp4_name)
        self.input_dir = cfg.VIDEO_SOURCE_PATH
        os.makedirs(self.input_dir, exist_ok=True)
        self.input_filename = mp4_name
        self.input_fileformat = mp4_ext
        self.input_path = self.input_dir + self.input_filename + self.input_fileformat


        self.output_dir = cfg.FRAMES_PATH + self.input_filename + cfg.SEP
        if(os.path.isdir(self.output_dir)):
            shutil.rmtree(self.output_dir)
        os.makedirs(self.output_dir, exist_ok=True)
        self.output_filename = 'frame%05d'
        self.output_fileformat = self.frames_extension
        self.output_path = self.output_dir + self.output_filename + self.output_fileformat
        
        # ffmpeg conversion start
        p = self.popenAndCall(self.ffmpeg_conv_cb, ['ffmpeg', '-loglevel', cfg._ffmpeg_loglevel, '-y', '-i', self.input_path, '-vf', self.scale, '-r', self.framerate, '-f', 'image2', self.output_path], shell=True)
        p.join()
            
    def deleteFrames(self, vid_file):
        shutil.rmtree(cfg.FRAMES_PATH+vid_file[:-4], ignore_errors=True)

    def generateThumb(self, v_file):
        # print('generating thumb for: {}'.format(v_file))
        name_wo_ext = os.path.splitext(v_file)[0]
        thumb_capture_frame = 100
        try:
            max_frames = int(cfg._vid_meta[name_wo_ext]['frames'])
        except Exception as e:
            print('error capturing max_frames: {}'.format(e))
        if(int(max_frames) > 0):
        # pprint(v_file[:-4])
            max_frames = int(cfg._vid_meta[name_wo_ext]['frames'])
            thumb_capture_frame = max_frames / 3
            # pprint(cfg._vid_meta[v_file[:-4]])
            pass
        # quit()
        # p = self.popenAndCall(self.thumb_generation_cb, ['ffmpeg', '-loglevel', cfg._ffmpeg_loglevel, '-y', '-ss', '00:00:10', '-i', cfg.VIDEO_SOURCE_PATH+v_file, '-vframes', '1', '-f', 'image2',  cfg.THUMB_PATH+v_file[:-4]+cfg._thumb_extension], shell=True)
        p = self.popenAndCall(self.thumb_generation_cb, ['ffmpeg', '-loglevel', cfg._ffmpeg_loglevel, '-y', '-i', cfg.VIDEO_SOURCE_PATH+v_file, '-vf', 'select=gte(n\,'+ str(thumb_capture_frame) +')', '-vframes', '1', '-f', 'image2', cfg.THUMB_PATH+v_file[:-4]+cfg._thumb_extension], shell=True)
        p.join()
        
    def thumb_generation_cb(*args):
        if(cfg._debug):
            print('thumb_generation_cb')
        
    def deleteThumb(self, t_file):
        # print('Deleting thumb: {}'.format(t_file))
        try:
            os.remove(cfg.THUMB_PATH+t_file)
        except Exception as e:
            print('Error removing thumb: {}'.format(e))

    def createZip(self, vid_file):
        mp4_name = (vid_file[:-4])
        mp4_ext = (vid_file[-4:])
        zf = ZipFile(cfg.ZIPS_PATH+mp4_name+'.zip', mode='w')
        
        path = cfg.FRAMES_PATH + mp4_name
        frames = [f for f in listdir(path) if isfile(join(path, f)) and f.endswith(self.frames_extension)]
        if(cfg._debug):
            print('creating archive for: {}'.format(vid_file))
        try:
            for x in frames:
                # print('adding file: {}'.format(path + cfg.SEP + x))
                zf.write(path + cfg.SEP + x, arcname=x)
        finally:
            zf.close()

    def getMetas(self):
        if not os.path.exists(cfg.TMP_PATH):
            os.makedirs(cfg.TMP_PATH)
        self.metainfo_path = cfg.TMP_PATH + 'temp_meta.ini'
        for vid_file in self.video_files:
            self.f_meta = open(self.metainfo_path, "w")
            # write a temp txt file for video metainfo 
            
            self.input_dir = cfg.VIDEO_SOURCE_PATH
            os.makedirs(self.input_dir, exist_ok=True)
            
            mp4_name = (vid_file[:-4])
            mp4_ext = os.path.splitext(vid_file)[1]
            self.input_filename = mp4_name
            self.input_fileformat = mp4_ext
            self.input_path = self.input_dir + self.input_filename + self.input_fileformat
            p = self.popenAndCall(self.ffmpeg_info_cb, ['ffprobe', '-v', 'error', '-select_streams', 'v:0', '-show_entries', 'stream=duration:stream=avg_frame_rate:stream=nb_frames', self.input_path], shell=True, universal_newlines=True, stdout=self.f_meta)
            p.join()
            
        return self.allMetas
        
    def writeToIni(self, allMetas):
        config = configparser.ConfigParser()
        for x in allMetas:
            # pprint(x[list(x.keys())[0]]['duration'])
            config[x[list(x.keys())[0]]['name']] = x[list(x.keys())[0]]
        
        with open(cfg._videos_metainfo_file, 'w') as configfile:
            config.write(configfile)
        
    def ffmpeg_conv_cb(self):
        if(cfg._debug):
            print('ffmpeg_conv_cb done!')
        
    def ffmpeg_info_cb(self):
        if(cfg._debug):
            print('reading info ffmpeg_info_cb...')
        self.f_meta.close() # close file after metainfo write

        meta_config = configparser.ConfigParser()
        meta_config.read(self.metainfo_path)
    
        duration_str = meta_config['STREAM']['duration']
        fps_str = meta_config['STREAM']['avg_frame_rate']
        frames_str = meta_config['STREAM']['nb_frames']
        
        if(frames_str != 'N/A'):
            frames = eval(frames_str)
        else:
            frames = -1
            
        if(duration_str != 'N/A'):
            duration = round(float(duration_str),2)
        else:
            duration = -1
            
        if(fps_str != 'N/A'):
            fps = eval(fps_str)
        else:
            fps = -1
        extension = self.input_fileformat
        
        metainfo = {}
        metainfo[self.input_filename] = {
            'name' : self.input_filename,
            'duration' : duration,
            'fps' : fps,
            'frames' : frames,
            'extension' : extension,
            'last_pos' : 0
            }

        self.allMetas.append(metainfo)
    
    def meta_sanity_check(self):
        # generate new videos_meta file if none exists
        if not os.path.exists(cfg._videos_metainfo_file):
            if(cfg._debug):
                print('Generate New meta - no file exists')
            self.writeMetaIni()
        
        readMetaIntoObject()
        
        # if readout from videos_meta is empty
        if not cfg._vid_meta:
            if(cfg._debug):
                print('Generate New meta - empty dict')
            self.writeMetaIni()
            readMetaIntoObject()
            
        # check if video filenames match the ini, generate new videos_meta file if mismatch
        compare_meta_list = []
        for m,n in cfg._vid_meta.items():
            compare_meta_list.append(m+n['extension'])
        
        if not compare(compare_meta_list, cfg._video_files):
            if(cfg._debug):
                print('Generate New meta - nomatch')
            self.writeMetaIni()
            readMetaIntoObject()

    def writeMetaIni(self):
        all_metas = self.getMetas()
        self.writeToIni(all_metas)

    @staticmethod
    def popenAndCall(onExit, *popenArgs, **popenKWArgs):
        """
        Runs a subprocess.Popen, and then calls the function onExit when the
        subprocess completes.

        Use it exactly the way you'd normally use subprocess.Popen, except include a
        callable to execute as the first argument. onExit is a callable object, and
        *popenArgs and **popenKWArgs are simply passed up to subprocess.Popen.
        """
        def runInThread(onExit, popenArgs, popenKWArgs):
            proc = subprocess.Popen(*popenArgs, **popenKWArgs)
            proc.wait()
            onExit()
            return

        thread = threading.Thread(target=runInThread, args=(onExit, popenArgs, popenKWArgs))
        thread.start()

        return thread # returns immediately after the thread starts


# START HELPERS

compare = lambda x, y: collections.Counter(x) == collections.Counter(y)

def writeSingleToIni(vid_name, field_name, field_value):
        config = configparser.ConfigParser()
        config.read(cfg.INI_PATH + 'videos_meta.ini')
        # pprint(x[list(x.keys())[0]]['duration'])
        # config[vid_name][field_name] = field_value
        config.set(vid_name, str(field_name), str(field_value))
        
        with open(cfg._videos_metainfo_file, 'w') as configfile:
            config.write(configfile)

def readMetaIntoObject():
    # read videos_meta into object
    meta_config = configparser.ConfigParser()
    meta_config.read(cfg._videos_metainfo_file)

    for x in meta_config.sections():
        _meta_name          = meta_config[x]['name']
        _meta_duration      = meta_config[x]['duration']
        _meta_fps           = meta_config[x]['fps']
        _meta_frames        = meta_config[x]['frames']
        _meta_extension     = meta_config[x]['extension']
        _last_pos           = meta_config[x]['last_pos']
        cfg._vid_meta[_meta_name] = {'duration' : _meta_duration,
         'fps' : _meta_fps,
         'frames' : _meta_frames,
         'extension' : _meta_extension,
         'last_pos' : _last_pos,
         }


# END HELPERS

if __name__ == "__main__":
    pass
    media = MediaMeta()
    all_metas = media.getMetas()
    # pprint(all_metas)
    media.writeToIni(all_metas)

    media.meta_sanity_check()
    # if(createFramesZips):
    for x in cfg._video_files:
        pprint(x)
        media.generateThumb(x)
    # quit()   
    
    # media.generateFrames()
    #     media.createZips()
