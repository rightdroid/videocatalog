import os
from os import listdir
from os.path import isfile, join
from pprint import pprint

colors = dict(
    white = (1, 1, 1, 1),
    red = (0.88, 0, 0.11, 1),
    blue = (0, 0.08, 0.16, 1),
    gray = (0.35, 0.35, 0.35, 1)
    )


# mail info

SEP = os.sep
ROOT_PATH           = os.path.dirname(os.path.realpath(__file__)) + SEP
SRC_PATH            = ROOT_PATH + 'src' + SEP
IMG_PATH            = SRC_PATH + 'img' + SEP
TMP_PATH            = ROOT_PATH + 'tmp' + SEP
INI_PATH            = ROOT_PATH
LOG_PATH            = ROOT_PATH + 'logs' + SEP
THUMB_PATH          = ROOT_PATH + 'thumbs' + SEP
FRAMES_PATH         = TMP_PATH + 'frames' + SEP
CAPS_PATH           = ROOT_PATH + 'caps' + SEP


# VIDEO_SOURCE_PATH   = SRC_PATH + 'frames' + SEP
VIDEO_SOURCE_PATH   = 'D:'+SEP+'Downloads'+SEP+'srv-1'+SEP+'1'+SEP

EXTENRAL_PLAYER = 'D:'+SEP+'Program Files'+SEP+'The KMPlayer'+SEP+'KMPlayer.exe'

# sainty check to create paths if they don't exist
try:
    if not os.path.exists(SRC_PATH): os.makedirs(SRC_PATH)
    if not os.path.exists(IMG_PATH): os.makedirs(IMG_PATH)
    if not os.path.exists(TMP_PATH): os.makedirs(TMP_PATH)
    if not os.path.exists(INI_PATH): os.makedirs(INI_PATH)
    if not os.path.exists(LOG_PATH): os.makedirs(LOG_PATH)
    if not os.path.exists(THUMB_PATH): os.makedirs(THUMB_PATH)
    if not os.path.exists(FRAMES_PATH): os.makedirs(FRAMES_PATH)
    if not os.path.exists(CAPS_PATH): os.makedirs(CAPS_PATH)
except Exception as e:
    print('Error creating paths: {}'.format(e))


_thumb_files = []
_thumb_files_wo_ext = []
_video_extensions = ('.mp4', '.3gp', '.avi', '.flv')
_vid_limit = False

_logging                    = True; # save logger messages to log file
_debug                      = False; # show logger messages
_inspector                  = True; # use kivy inspector

_main_monitor_w = '1366'
_main_monitor_h = '768'


_ffmpeg_loglevel        = 'panic' # default panic, change to 'debug' for debug 
_thumb_extension        = '.jpg' # format for bg images used on recording countdown
_frames_extension       = '.png' # format for frame imges used in zips
_frames_scale           = 'scale=320:-1' # dimensions of frame image (x:y)
_frames_fps             = str(20) # frames per second to extract frames from video

_video_files = []
_video_files_wo_ext = []

_videos_metainfo_file   = INI_PATH + 'videos_meta.ini'

_vid_duration = {}
_vid_meta = {}


_current_selection = ''
_new_vid = ''


# timeout and iteration control
_ffmpeg_done = False


# GENERAL

general = {
    'lang' : 'eng',
}


# VISUAL

#fonts
font_main = 'Exo2-Medium.ttf'
font_exo = 'Exo2-Black.ttf'
font_unicode = 'DejaVuSans.ttf' # fallback font for special characters
font_size_xs = '15sp' # keyboard button
font_size_sm = '20sp' # keyboard button
font_size_kb = '30sp' # keyboard button
font_size_kb_large = '35sp'
font_size_xlarge = '40sp' # on confirmation screen
font_size_countdown = '500sp' # on confirmation screen
